# install.packages('reshape2')
# install.packages('readxl')
# install.packages('dplyr')
# install.packages('foreign')
# install.packages('tidyr')
# install.packages('devtools')
# install.packages('mice')
# install.packages('mltools')
# install.packages('data.table')

# install.packages('RSiena')
# install.packages('Matrix')
# install.packages('ROSE')
# install.packages('PRROC')
# install.packages('parallel')


setwd("~/src/thesis_data")
library(reshape2)
library(readxl)
library(foreign)
library(tidyr)
library(dplyr)
library(devtools)
library(mice)
library(stringr)

saveRDS(df.dyadic, "./rds_sets/df.dyadic.rds")
df.dyadic.a <- readRDS("./rds_sets/df.dyadic.rds")

start_year <- 1945
end_year <- 2015

##########################
# Dyadic Dataset
##########################

# contiguity: COW Direct Contiguity

data.contiguity <- read.csv("data_sets/dyadic/contdird.csv")
df.dyadic <- data.contiguity[c('state1no', 'state2no', 'year', 'conttype')]
df.dyadic <- df.dyadic %>%
  filter(year > start_year & year < end_year) %>%
  mutate(
    conttype = 1
  ) %>%
  rename(
    "contiguity" = "conttype"
  )

# alliance: COW Formal Alliance

data.alliance <- read.csv("data_sets/dyadic/alliance_v4.1_by_dyad_yearly.csv")
data.alliance <- data.alliance[c('ccode1', 'ccode2', 'year')]
data.alliance <- data.alliance %>%
  mutate(alliance = 1) %>%
  filter(year > start_year & year < end_year)

data.alliance.reverse <- data.alliance %>%
  mutate(
    ccode.trans = ccode2,
    ccode2 = ccode1,
    ccode1 = ccode.trans,
  ) %>%
  subset(select=-c(ccode.trans))

data.alliance.symmetric <- rbind(data.alliance, data.alliance.reverse) 

df.dyadic <- merge(x=df.dyadic,
                   y=data.alliance.symmetric,
                   by.x=c('state1no', 'state2no', 'year'),
                   by.y=c('ccode1', 'ccode2', 'year'),
                   all=TRUE)

# trade: COW International Trade, 1870-2014 (v4.0)

data.trade.raw <- read.csv("data_sets/dyadic/Dyadic_COW_4.0.csv")
data.trade <- data.trade.raw[c('ccode1', 'ccode2', 'year', 'flow1', 'flow2')]
data.trade <- data.trade %>% rename(
  'trade_exports' = 'flow1',
  'trade_imports' = 'flow2'
)

data.trade <- data.trade %>% 
  filter(year > start_year & year < end_year) %>%
  mutate(across(c(trade_exports, trade_imports), na_if, -9))

data.trade.mice <- data.trade %>% mutate(
  ccode1 = as.factor(ccode1),
  ccode2 = as.factor(ccode2)
)

data.trade.miced <- complete(mice(data.trade.mice, m=1))

data.trade.reversed <- data.trade.miced %>% mutate(
  ccode1.trans = ccode2,
  ccode2 = ccode1,
  ccode1 = ccode1.trans,
  trade_imports.trans = trade_exports,
  trade_exports = trade_imports,
  trade_imports = trade_imports.trans,
) %>% select(ccode1, ccode2, year, trade_exports, trade_imports)

data.trade.symmetric <- rbind(data.trade.miced, data.trade.reversed)
data.trade.symmetric$trade_aggregate <- data.trade.symmetric$trade_exports + data.trade.symmetric$trade_imports

data.trade.symmetric <- data.trade.symmetric %>% filter(data.trade.symmetric$trade_aggregate > 0)

trade.country_year <- data.trade.symmetric %>%
  group_by(ccode1, year) %>%
  summarise(total_trade = sum(trade_aggregate))

data.trade.symmetric$trade_percent <- 0

data.trade.symmetric <- data.trade.symmetric %>% rowwise() %>% mutate(
  trade_percent = trade_aggregate/trade.country_year[trade.country_year$ccode1 == ccode1 &
                                                       trade.country_year$year == year,]$total_trade
)

trade.year <- data.trade.symmetric %>%
  group_by(year) %>%
  summarise(trade_volume = quantile(trade_aggregate, 0.8),
            trade_dependent = quantile(trade_percent, 0.8, na.rm=TRUE))

data.trade.symmetric <- data.trade.symmetric %>% mutate(
  high_volume_trade = if_else(trade_aggregate > trade.year[trade.year$year == year, "trade_volume"], 1, 0),
  high_dependence_trade = if_else(trade_percent > trade.year[trade.year$year == curr_year, "trade_dependent"], 1, 0)
)

data.trade.symmetric <- data.trade.symmetric %>%
  filter(high_volume_trade == 1 | high_dependence_trade == 1) %>%
  select(c("ccode1", "ccode2", "year", "high_volume_trade", "high_dependence_trade"))

df.dyadic <- merge(x=df.dyadic,
                   y=data.trade.symmetric,
                   by.x=c('state1no', 'state2no', 'year'),
                   by.y=c('ccode1', 'ccode2', 'year'),
                   all=TRUE)

# # HMI: Humanitarian Military Intervention (N. Bosworth)
# 
# data.HMI <- read_xlsx("data_sets/dyadic/HMI Research.xlsx")
# data.HMI.targets <- data.HMI[c("YEAR", "COWCC" ,"NOFLY", "REGIME", "FATALITYIY", "GROUNDNO", "UNSC")]
# data.HMI.interveners <- data.HMI %>% select(grep("I[0-9]*CCC", names(data.HMI)))
# data.HMI.sub <- cbind(data.HMI.targets, data.HMI.interveners)
# data.HMI.sub <- na.omit(data.HMI.sub)
# 
# # Melt to create edge list
# data.interventions <- melt(data=data.HMI.sub, id.vars=c("YEAR", "COWCC", "NOFLY", "REGIME", "FATALITYIY", "GROUNDNO", "UNSC"), measure.vars=names(data.HMI.interveners))
# # Limit data to just variables of interest
# data.interventions <- data.interventions[c("YEAR", "COWCC", "value", "NOFLY", "REGIME", "FATALITYIY", "GROUNDNO", "UNSC")]
# 
# # Remove edges with values coded for no data/no intervener: Delete -99, -88 and all cowids > 999
# data.interventions$COWCC <- as.numeric(data.interventions$COWCC)
# data.interventions$value <- as.numeric(data.interventions$value)
# data.interventions <- data.interventions[!(data.interventions$value == -88),]
# data.interventions <- data.interventions[!(data.interventions$value == -99),]
# data.interventions <- data.interventions[!(data.interventions$value > 999),]
# 
# # Delete duplicates
# data.interventions <- unique(data.interventions)
# data.interventions <- data.interventions %>% na_if(-99)
# data.interventions <- data.interventions %>% na_if(-88)
# 
# data.interventions <- data.interventions[c('YEAR', 'COWCC', 'value')]
# data.interventions$intervention <- 1
# 
# df.dyadic <- merge(x=df.dyadic,
#                    y=data.interventions,
#                    by.x=c('state1no', 'state2no', 'year'),
#                    by.y=c('value', 'COWCC', 'YEAR'),
#                    all=TRUE)

# Cooperation between states and rebel groups
data.rebel_support.raw <- read.csv("data_sets/dyadic/rebel_support.csv")
data.rebel_support <- data.rebel_support.raw %>%
  mutate(
    rebel_support = 1,
    SupNum_COW = ifelse(SupNum_COW %in% c(".", "", "0"), NA, SupNum_COW)
  ) %>%
  drop_na(SupNum_COW) %>% 
  filter(
    (S_SafeMem == "1" & S_Precision_SM != "4") |
    (S_SafeLead == "1" & S_Precision_SL != "4") |
    (S_Offices == "1" & S_Precision_Head != "4") |
    (S_TrainCamp == "1" & S_Precision_TC != "4") |
    (S_Training == "1" & S_Precision_Train != "4") | 
    (S_WeaponLog == "1" & S_Precision_WL != "4") | 
    (S_FinAid == "1" & S_Precision_FinAid != "4") | 
    (S_Transport == "1" & S_Precision_Tp != "4") | 
    (S_Troop == "1" & S_Precision_Tr != "4") | 
    (S_Other == "1" & S_Precision_Oth != "4") 
  ) %>% select(c('SupNum_COW', 'TarNum_COW', 'Year', "rebel_support"))

df.dyadic <- merge(x=df.dyadic,
                   y=data.rebel_support,
                   by.x=c('state1no', 'state2no', 'year'),
                   by.y=c('SupNum_COW', 'TarNum_COW', 'Year'),
                   all=TRUE)

data.UN_Nations <- read.csv("Untitled.csv")
data.UN_Nations <- data.UN_Nations %>% rowwise %>% mutate(
  Country = str_sub(Country, 2),
  Date = str_extract_all(Date, regex("[0-9]{4}"))[[1]][1]
)

# Sanctions
renameCountries <- function(x, year) {
  x <- sub("Yemen, North", "North Yemen", x)
  x <- sub("Congo, Democratic Republic of the", "Democratic Republic of the Congo", x)
  x <- sub("Korea, North", "North Korea", x)
  x <- sub("Gambia, The", "The Gambia", x)
  x <- sub("Korea, South", "South Korea", x)
  x <- sub("Egypt, Arab Rep.", "Egypt", x)
  
  EU_nations <- ""
  if (year >= 2013) {
    EU_nations <- "Croatia, "
  }
  if (year >= 2007) {
    EU_nations <- paste(EU_nations, "Romania, Bulgaria, ", sep="")
  }
  if (year >= 2004) {
    EU_nations <- paste(EU_nations, "Poland, Hungary, Czech Republic, Slovakia, Lithuania, Latvia, Slovenia, Estonia, Cyprus, Malta, ", sep="")
  }
  if (year >= 1995) {
    EU_nations <- paste(EU_nations, "Sweden, Austria, Finland, ", sep="")
  }
  if (year >= 1986) {
    EU_nations <- paste(EU_nations, "Spain, Portugal, ", sep="")
  }
  if (year >= 1981) {
    EU_nations <- paste(EU_nations, "Greece, ", sep="")
  }
  if (year >= 1973) {
    EU_nations <- paste(EU_nations, "Denmark, Ireland, ", sep="")
  }
  
  EU_nations <- paste(EU_nations, "Germany, France, Italy, Netherlands, Belgium, Luxembourg", sep="")
  
  x <- sub("EU", EU_nations, x)
  
  AU_nations <- ""
  if (year >= 2011) {
    AU_nations <- "South Sudan, "
  }
  if (year >= 1994) {
    AU_nations <- paste(AU_nations, "South Africa, ", sep="")
  }
  if (year >= 1993) {
    AU_nations <- paste(AU_nations, "Eritrea, ", sep="")
  }
  if (year >= 1990) {
    AU_nations <- paste(AU_nations, "Namibia, ", sep="")
  }
  if (year >= 1982) {
    AU_nations <- paste(AU_nations, "Sahrawi Arab Democratic Republic, ", sep="")
  }
  if (year >= 1980) {
    AU_nations <- paste(AU_nations, "Zimbabwe, ", sep="")
  }
  if (year >= 1979) {
    AU_nations <- paste(AU_nations, "Angola, ", sep="")
  }
  if (year >= 1977) {
    AU_nations <- paste(AU_nations, "Djibouti, ", sep="")
  }
  if (year >= 1976) {
    AU_nations <- paste(AU_nations, "Seychelles, ", sep="")
  }
  if (year >= 1975) {
    AU_nations <- paste(AU_nations, "São Tomé and Príncipe, Mozambique, Comoros, Cape Verde, ", sep="") # Fill
  }
  if (year >= 1973) {
    AU_nations <- paste(AU_nations, "Guinea-Bissau, ", sep="") # Fill
  }
  if (year >= 1968) {
    AU_nations <- paste(AU_nations, "Equatorial Guinea, Eswatini, Mauritius, ", sep="") # Fill
  }
  if (year >= 1966) {
    AU_nations <- paste(AU_nations, "Lesotho, Botswana, ", sep="") # Fill
  }
  if (year >= 1965) {
    AU_nations <- paste(AU_nations, "Gambia, ", sep="") # Fill
  }
  if (year >= 1964) {
    AU_nations <- paste(AU_nations, "Zambia, Malawi, ", sep="") # Fill
  }
  
  AU_nations <- paste(AU_nations, "Kenya, Uganda, Tunisia, Togo, Tanzania, Sudan, Somalia, Sierra Leone, Senegal, Rwanda, Nigeria, Niger, Morocco, Mauritania, Mali, Madagascar, Libya, Liberia, Ivory Coast, Guinea, Ghana, Gabon, Ethiopia, Egypt, Republic of the Congo, Democratic Republic of the Congo, Chad, Central African Republic, Cameroon, Burundi, Burkina Faso, Benin, Algeria", sep="")

  x <- sub("African Union", AU_nations, x)
  
  UN_nations <- data.UN_Nations %>% filter(year >= Date)
  UN_nations <- UN_nations$Country
  UN_nations <- paste(UN_nations, collapse=", ")
  
  x <- sub("UN", UN_nations, x)
  
  x
}

data.sanctions.raw <- read_xls("data_sets/dyadic/GSDB_V2/GSDB_V2.xls")
data.sanctions.renamed <- data.sanctions.raw %>% rowwise() %>% mutate(
  sanctioned_state = renameCountries(sanctioned_state, begin),
  sanctioning_state = renameCountries(sanctioning_state, begin)
)

data.sanctions.ego_commas <- data.sanctions.renamed[grepl(",", data.sanctions.renamed$sanctioning_state, fixed=TRUE), ]

data.sanctions.new <- setdiff(data.sanctions.renamed, data.sanctions.ego_commas)

for (i in 1:nrow(data.sanctions.ego_commas)) {
  curr_row <- data.sanctions.ego_commas[i, ]
  
  curr_recievers <- as.data.frame(str_split(curr_row$sanctioning_state, ", "))
  
  for (j in 1:nrow(curr_recievers)) {
    new_row <- curr_row
    new_row$sanctioning_state <- curr_recievers[j,1]
    
    data.sanctions.new <- rbind(data.sanctions.new, new_row)
  }
}

data.sanctions.alter_commas <- data.sanctions.new[grepl(",", data.sanctions.new$sanctioned_state, fixed=TRUE), ]
data.sanctions.new <- setdiff(data.sanctions.new, data.sanctions.alter_commas)

for (i in 1:nrow(data.sanctions.alter_commas)) {
  curr_row <- data.sanctions.alter_commas[i, ]
  
  curr_recievers <- as.data.frame(str_split(curr_row$sanctioned_state, ", "))
  
  for (j in 1:nrow(curr_recievers)) {
    new_row <- curr_row
    new_row$sanctioned_state <- curr_recievers[j,1]
    
    data.sanctions.new <- rbind(data.sanctions.new, new_row)
  }
}

data.COW <- read.csv("data_sets/COW_country_codes.csv")
data.COW <- unique(data.COW)

nameToCcode <- function(country_name) {
  if (country_name == "United States") {
    return (2)
  } else if (country_name == "South Vietnam") {
    return (817)
  } else if (country_name == "North Yemen") {
    return (678)
  } else if (country_name == "Soviet Union") {
    return (365)
  } else if (country_name == "Rhodesia") {
    return (552)
  } else if (country_name == "Palestine") {
    return (666)
  } else if (country_name == "Malagasy Republic") {
    return (580)
  } else if (country_name == "The Gambia") {
    return (420)
  } else if (country_name == "Ethiopia (excludes Eritrea)") {
    return (530)
  } else if (country_name == "Cote d'Ivoire") {
    return (437)
  } else if (country_name == "Congo (Brazzaville)") {
    return (490)
  } else if (country_name == "North Vietnam") {
    return (816)
  } else if (country_name == "Malaya") {
    return (820)
  } else if (country_name == "Gibraltar") {
    return (230)
  } else if (country_name == "Cyprus (Northern)") {
    return (352)
  } else if (country_name == "Ceylon") {
    return (780)
  } else if (country_name == "Antigua and Barbuda") {
    return (58)
  } else if (country_name == "Serbia") {
    return (345)
  } else if (country_name == "Republic of the Congo") {
    return (490)
  } else if (country_name == "Eswatini") {
    return (572)
  } else if (country_name == " Norway") {
    return (385)
  } else if (country_name == "São Tomé and Príncipe") {
    return (403)
  } else if (country_name == "Sahrawi Arab Democratic Republic") {
    return (600)
  } else if (country_name == " Sweden") {
    return (380)
  } else if (country_name == " Slovenia") {
    return (349)
  } else if (country_name == "Hong Kong") {
    return (710)
  }
  
  curr <- data.COW[data.COW$StateNme==country_name,]
  
  if (nrow(curr) == 0) {
    print(paste("couldn't find: ", country_name))
    return (NA)
  }
  
  curr$CCode
}

data.sanctions.new <- as.data.frame(data.sanctions.new)
data.sanctions.new_ <- data.sanctions.new %>% rowwise() %>% mutate(
  sanctioning_state = nameToCcode(sanctioning_state),
  sanctioned_state = nameToCcode(sanctioned_state)
)

data.sanctions.dyadic <- data.frame(ccode_ego= numeric(0), ccode_alter=numeric(0), year= numeric(0), sanc.trade=numeric(0), sanc.arms=numeric(0), sanc.military=numeric(0), sanc.financial=numeric(0), sanc.travel=numeric(0))

for (i in 1:nrow(data.sanctions.new)) {
  curr_row <- data.sanctions.new[i,]
  
  for (year in curr_row$begin:curr_row$end) {
    data.sanctions.dyadic[nrow(data.sanctions.dyadic)+1,] <- c(
      curr_row$sanctioning_state,
      curr_row$sanctioned_state,
      year,
      curr_row$trade,
      curr_row$arms,
      curr_row$military,
      curr_row$financial,
      curr_row$travel
    )
  }
}

data.sanctions.dyadic <- data.sanctions.dyadic %>% filter(!(is.na(ccode_ego) | is.na(ccode_alter)))


df.dyadic <- merge(x=df.dyadic,
                   y=data.sanctions.dyadic,
                   by.x=c('state1no', 'state2no', 'year'),
                   by.y=c('ccode_ego', 'ccode_alter', 'year'),
                   all=TRUE)

#Dyadic Cleanup
df.dyadic <- df.dyadic %>% filter(year > start_year & year < end_year)

df.dyadic <- df.dyadic[df.dyadic$state1no != df.dyadic$state2no,]

df.dyadic <- df.dyadic %>% mutate(
  state1no = as.integer(state1no),
  state2no = as.integer(state2no),
  contiguity = replace_na(contiguity, 0),
  alliance = replace_na(alliance, 0),
  high_volume_trade = replace_na(high_volume_trade, 0),
  high_dependence_trade = replace_na(high_dependence_trade, 0),
  intervention = replace_na(intervention, 0),
  rebel_support = replace_na(rebel_support, 0),
  sanc.trade = replace_na(sanc.trade, 0),
  sanc.arms = replace_na(sanc.arms, 0),
  sanc.military = replace_na(sanc.military, 0),
  sanc.financial = replace_na(sanc.financial, 0),
  sanc.travel = replace_na(sanc.travel, 0)
)
