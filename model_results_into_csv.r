setwd("~/src/thesis_data")
currLib <- "~/src/Rlib"
.libPaths(c(.libPaths(), currLib))
library(RSiena, lib.loc=currLib)

model_results <- list.files("./model_results/res")

mod_res.all <- list()

for (mod_res.name in model_results) {
  mod_res.file_path <- paste("./model_results/res/",mod_res.name,sep="")
  
  mod_res <- as.data.frame(readRDS(mod_res.file_path))
  mod_res <- as.data.frame(mod_res[,1])
  colnames(mod_res) <- mod_res.name
  mod_res.all <- c(mod_res.all, mod_res)
}

write.csv(mod_res.all, "./model_results.csv")
