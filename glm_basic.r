setwd("~/src/thesis_data")

library(dplyr)

df.monadic <- readRDS("./rds_sets/df.monadic.new.rds")
df.dyadic <- readRDS("./rds_sets/df.dyadic.rds")

ccodes <- unique(append(df.dyadic$state1no, df.dyadic$state2no))
df.monadic <- df.monadic[df.monadic$ccode %in% ccodes, ]

df.monadic <- df.monadic %>% mutate(
  tpop = (scale(df.monadic$tpop) %>% as.vector),
  milper = (scale(df.monadic$milper) %>% as.vector),
  cinc = (scale(df.monadic$cinc) %>% as.vector),
  ifhpol = -(scale(df.monadic$ifhpol) %>% as.vector),
  IMR = (scale(df.monadic$IMR) %>% as.vector),
  EFindex = (scale(df.monadic$EFindex) %>% as.vector)
)

df.monadic <- complete(mice(df.monadic))

roc_c <- list()
prc_c <- list()

for (start_year in 1947:1987) {
  training_window <- 20
  testing_window <- 5
  
  df.monadic.train <- df.monadic[df.monadic$year >= start_year & df.monadic$year <= start_year+training_window, ]
  df.monadic.test <- df.monadic[df.monadic$year > start_year+training_window & df.monadic$year <= start_year+training_window+testing_window,]
  
  my_glm <- suppressWarnings(glm(tmk.future ~ tpop+milper+cinc+ifhpol+IMR+EFindex, family="binomial", data=df.monadic.train))
  # summary(my_glm)
  
  prob <- predict(my_glm, newdata=df.monadic.test, type="response")
  df.monadic.test$prob <- prob
  
  g <- suppressMessages(roc(df.monadic.test$tmk.future, df.monadic.test$prob))
  print(paste(start_year, auc(g)))
  
  score1 <- df.monadic.test[df.monadic.test$tmk.future == 1,"prob"]
  score0 <- df.monadic.test[df.monadic.test$tmk.future == 0,"prob"]
  
  roc <- roc.curve(score1, score0, curve=TRUE)
  prc <- pr.curve(score1, score0, curve=TRUE)
  
  roc_c <- c(roc_c, prc$auc.davis.goadrich)
  prc_c <- c(prc_c, prc$auc.davis.goadrich)
}

write.csv(roc_c, "./GLM_basic_roc.csv")
write.csv(prc_c, "./GLM_basic_prc.csv")
