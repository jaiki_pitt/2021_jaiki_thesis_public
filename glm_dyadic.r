setwd("~/src/thesis_data")

library(dplyr)
library(pROC)
library(mice)
library(performance)

df.monadic <- readRDS("./rds_sets/df.monadic.new.rds")
df.dyadic <- readRDS("./rds_sets/df.dyadic.rds")

for (i in (1:15)) {
  df.monadic.temp <- readRDS(paste("./rds_sets_new/df.monadic.with_dyad_",i,".rds", sep=""))
  df.monadic.temp <- df.monadic.temp %>% select(c(1, 2, ncol(df.monadic.temp)))
  
  df.monadic <- merge(
    x=df.monadic,
    y=df.monadic.temp,
    by=c("ccode", "year")
  )
}

ccodes <- unique(append(df.dyadic$state1no, df.dyadic$state2no))
df.monadic <- df.monadic[df.monadic$ccode %in% ccodes, ]

df.monadic <- df.monadic %>% mutate(
  tpop = (scale(df.monadic$tpop) %>% as.vector),
  milper = (scale(df.monadic$milper) %>% as.vector),
  cinc = (scale(df.monadic$cinc) %>% as.vector),
  ifhpol = -(scale(df.monadic$ifhpol) %>% as.vector),
  IMR = (scale(df.monadic$IMR) %>% as.vector),
  EFindex = (scale(df.monadic$EFindex) %>% as.vector),
  indeg_alliance = (scale(df.monadic$indeg_alliance) %>% as.vector),
  recip_high_dependence_trade = (scale(df.monadic$recip_high_dependence_trade) %>% as.vector),
  outdeg_high_volume_trade = (scale(df.monadic$outdeg_high_volume_trade) %>% as.vector),
  avAlt_ifhpol_high_dependence_trade = (scale(df.monadic$avAlt_ifhpol_high_dependence_trade) %>% as.vector),
  avAlt_ifhpol_high_volume_trade = (scale(df.monadic$avAlt_ifhpol_high_volume_trade) %>% as.vector),
  avAlt_ifhpol_alliance = (scale(df.monadic$avAlt_ifhpol_alliance) %>% as.vector),
  outdeg_alliance = (scale(df.monadic$outdeg_alliance) %>% as.vector),
  avAlt_ifhpol_contiguity = (scale(df.monadic$avAlt_ifhpol_contiguity) %>% as.vector),
  totAlt_civil_war_contiguity = (scale(df.monadic$totAlt_civil_war_contiguity) %>% as.vector),
  indeg_rebel_support = (scale(df.monadic$indeg_rebel_support) %>% as.vector),
  totInAlt_nmc_rebel_support = (scale(df.monadic$totInAlt_nmc_rebel_support) %>% as.vector),
  indeg_sanc.military = (scale(df.monadic$indeg_sanc.military) %>% as.vector),
  totInAlt_nmc_sanc.military = (scale(df.monadic$totInAlt_nmc_sanc.military) %>% as.vector),
  indeg_sanc.economic = (scale(df.monadic$indeg_sanc.economic) %>% as.vector),
  totInAlt_nmc_sanc.economic = (scale(df.monadic$totInAlt_nmc_sanc.economic) %>% as.vector)
)

df.monadic$tpop[is.nan(df.monadic$tpop)] <- NA
df.monadic$milper[is.nan(df.monadic$milper)] <- NA
df.monadic$cinc[is.nan(df.monadic$cinc)] <- NA
df.monadic$ifhpol[is.nan(df.monadic$ifhpol)] <- NA
df.monadic$IMR[is.nan(df.monadic$IMR)] <- NA
df.monadic$EFindex[is.nan(df.monadic$EFindex)] <- NA
df.monadic$indeg_alliance[is.nan(df.monadic$indeg_alliance)] <- NA
df.monadic$recip_high_dependence_trade[is.nan(df.monadic$recip_high_dependence_trade)] <- NA
df.monadic$outdeg_high_volume_trade[is.nan(df.monadic$outdeg_high_volume_trade)] <- NA
df.monadic$avAlt_ifhpol_high_dependence_trade[is.nan(df.monadic$avAlt_ifhpol_high_dependence_trade)] <- NA
df.monadic$avAlt_ifhpol_high_volume_trade[is.nan(df.monadic$avAlt_ifhpol_high_volume_trade)] <- NA
df.monadic$avAlt_ifhpol_alliance[is.nan(df.monadic$avAlt_ifhpol_alliance)] <- NA
df.monadic$outdeg_alliance[is.nan(df.monadic$outdeg_alliance)] <- NA
df.monadic$avAlt_ifhpol_contiguity[is.nan(df.monadic$avAlt_ifhpol_contiguity)] <- NA
df.monadic$totAlt_civil_war_contiguity[is.nan(df.monadic$totAlt_civil_war_contiguity)] <- NA
df.monadic$indeg_rebel_support[is.nan(df.monadic$indeg_rebel_support)] <- NA
df.monadic$totInAlt_nmc_rebel_support[is.nan(df.monadic$totInAlt_nmc_rebel_support)] <- NA
df.monadic$indeg_sanc.military[is.nan(df.monadic$indeg_sanc.military)] <- NA
df.monadic$totInAlt_nmc_sanc.military[is.nan(df.monadic$totInAlt_nmc_sanc.military)] <- NA
df.monadic$indeg_sanc.economic[is.nan(df.monadic$indeg_sanc.economic)] <- NA
df.monadic$totInAlt_nmc_sanc.economic[is.nan(df.monadic$totInAlt_nmc_sanc.economic)] <- NA

df.monadic <- complete(mice(df.monadic))

auc_res <- list()
prc_res <- list()
pvalue_res <- list()
estimate_res <- list()

for (start_year in 1947:1987) {
  training_window <- 20
  testing_window <- 5
  
  df.monadic.train <- df.monadic[df.monadic$year >= start_year & df.monadic$year <= start_year+training_window, ]
  # df.monadic.new <- smote(tmk.future~., df.monadic.train, perc.over=2, perc.under=0)
  # df.monadic.train. <- rbind(df.monadic.train, df.monadic.new)
  df.monadic.train$tmk.future <- as.numeric(df.monadic.train$tmk.future)
  df.monadic.test <- df.monadic[df.monadic$year > start_year+training_window & df.monadic$year <= start_year+training_window+testing_window,]
  
  my_glm <- suppressWarnings(glm(tmk.future ~ tpop+milper+cinc+ifhpol+IMR+EFindex+
                                   indeg_alliance+recip_high_dependence_trade+outdeg_high_volume_trade+
                                   avAlt_ifhpol_high_dependence_trade+avAlt_ifhpol_high_volume_trade+
                                   avAlt_ifhpol_alliance+avAlt_ifhpol_contiguity+
                                   totAlt_civil_war_contiguity+
                                   indeg_rebel_support+totInAlt_nmc_rebel_support+
                                   indeg_sanc.military+totInAlt_nmc_sanc.military+
                                   indeg_sanc.economic+totInAlt_nmc_sanc.economic
                                 , family="binomial", data=df.monadic.train, na.action=na.exclude))
  
  # summary(my_glm)
  
  prob <- predict(my_glm, newdata=df.monadic.test, type="response")
  df.monadic.test$prob <- prob
  g <- suppressMessages(roc(df.monadic.test$tmk.future, df.monadic.test$prob))
  
  # pred <- prediction(prob, df.monadic.test$tmk.future)
  # perf <- performance(pred, "prec", "rec")
  
  score1 <- df.monadic.test[df.monadic.test$tmk.future == 1,"prob"]
  score0 <- df.monadic.test[df.monadic.test$tmk.future == 0,"prob"]

  # roc <- roc.curve(score1, score0, curve=TRUE)
  prc <- pr.curve(score1, score0, curve=TRUE)
  

  auc_res <- c(auc_res, auc(g))
  prc_res <- c(prc_res, prc$auc.davis.goadrich)
  estimate_res <- c(estimate_res, as.data.frame(coef(summary(my_glm))[,1]))
  pvalue_res <- c(pvalue_res, as.data.frame(coef(summary(my_glm))[,4]))
}

glm_labels <- labels(my_glm$terms)
write.csv(glm_labels, "./glm_dyadic_results/GLM_dyadic_labels.csv")
write.csv(auc_res, "./glm_dyadic_results/GLM_dyadic_auc.csv")
write.csv(prc_res, "./glm_dyadic_results/GLM_dyadic_prc.csv")
write.csv(estimate_res, "./glm_dyadic_results/GLM_dyadic_estimate.csv")
write.csv(pvalue_res, "./glm_dyadic_results/GLM_dyadic_Pval.csv")
