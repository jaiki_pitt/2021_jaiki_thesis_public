setwd("~/src/thesis_data")
currLib <- "~/src/Rlib"
.libPaths(c(.libPaths(), currLib))
library(RSiena, lib.loc=currLib)

for (i in 1947:1987) {
	curr_file <- paste("./model_results/sim_train(",i,").rds",sep="")
	
	try(curr_model <- readRDS(curr_file), silent=TRUE)

	a <- cbind(curr_model$theta[196:216], curr_model$effects[196:216,2,])	
	
	res_file <- paste("./model_results/res/res(",i,").rds",sep="")
	saveRDS(a, res_file)
}
# b <- cbind(a$theta[198:217], a$effects[198:217,2,])

