# Assessing The Effect Of International Relations On The Occurrence Of Targeted Mass Killing Events

## File Structure
- **data_sets** folder for raw data used
- **glm_dyadic_results** folder for results of flattening dyadic effects
- **jobs** folder for scripts to be run on Katana
- **model_resulst** folder for results of saom_model.r
- **rds_sets** folder for result of processing raw data into monadic and dyadic dataset
- **add_dyadic_effects_to_monadic** script to combine results of flattened dyadic effects into a single dataset
- **extract_results.r** script to extract SAOM coefficients from large Siena Objects into simple tables
- **glm_basic.r** script to run GLM Basic
- **glm_dyadic.r** script to run GLM Dyadic
- **model_results_into_csv.r** script to turn SAOM coefficient tables into readable .csv files
- **preprocessing_dyadic.r** script to process raw dyadic data into single dyadic dataset
- **preprocessing_monadic.r** script to process raw monadic data into single monadic dataset
- **saom_model.r** script to run SAOM model

## Usage

### Preprocessing

The preprocessing files combine raw datasets into single R data frames.

To create a new monadic R data object run command `Rscript preprocessing_monadic.r`. The result will be stored in `/rds_sets/` folder.

To create a new monadic R data object run command `Rscript preprocessing_dyadic.r`. The result will be stored in `/rds_sets/` folder.

To create a flattened dyadic data object for GLM Dyadic, Katana must be used. This is because the flattening process is computationally intensive.

1. SSH into Katana server.
2. Navigate to `/jobs/` folder.
3. Use command `qsub monadic_add_effects.pbs`

Results are stored in `/rds_sets/monadic_plus_dyadic/` folder. These objects are merged in `glm_dyadic.r`.

### GLM Basic

GLM Basic is a logistic regression that classifies observations (if there is a TMK event onset in 5 years) using monadic data.

Use command `Rscript glm_basic.r` to run GLM Basic

Metric results for run will be in GLM_basic_roc.csv and GLM_basic_prc.csv. The script does not store the coefficient estimate results.

### GLM Dyadic

GLM Dyadic is a logistic regression that classifies observations (if there is a TMK event onset in 5 years) using monadic data + dyadic data.

Use command `Rscript glm_dyadic.r` to run GLM Dyadic

Results for run will be in `/glm_dyadic_results/` folder. This includes metrics, coefficient estimates and p-values.

### SAOM

SAOM uses longitudinal network data to predict future TMK events (if there is a tmK event onset in 5 years).

To run a single year, use command `Rscript saom_model.r [window_start_year]`. A single run takes ~12 hours. This should only be done for testing/debugging.

In order to run the model for all windows, in a reasonable time, the model needs to be run through Katana.

1. SSH into Katana server.
2. Navigate to `/jobs/` folder.
3. Use command `qsub saom_window_array.pbs`

It should take ~2 days for all runs to complete.

It is normal for a few runs to not complete on the first attempt. This mostly due to runs not completing in their allocated time (each run must complete in a maximum of 24 hours). To ammend this, run the same `qsub saom_window_array.pbs` command and every window that didn't complete will be rerun. Nearly all models are successful on the second attempt as they use the previously completed runs as a guide on coefficient estimates.

#### Model Result Coefficient Estimates

The results of raw RSiena objects are stored in `/model_results/` folder. These are the results of the coefficient estimates, not metric results.

Use command `Rscript extract_results.r` to turn raw RSiena results to simple data frames. These data frames will be stored in `/model_results/res/`.

Use command `Rscript model_results_into_csv.r` to turn data frame results into .csv files.

#### Model Result Metric Results

The models' metric results are printed during the model runs. To view the results look in the `/jobs/` folder. The logs of each run will be stored here. The metrics are at the bottom of the file.
