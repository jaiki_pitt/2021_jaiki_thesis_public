# install.packages('reshape2')
# install.packages('readxl')
# install.packages('dplyr')
# install.packages('foreign')
# install.packages('tidyr')
# install.packages('devtools')
# install.packages('mice')
# install.packages('mltools')
# install.packages('data.table')

# install.packages('RSiena')
# install.packages('Matrix')
# install.packages('ROSE')
# install.packages('PRROC')
# install.packages('parallel')

setwd("~/src/thesis_data")
library(dplyr)
library(mice)
library(tidyr)
library(readxl)
library(foreign)

saveRDS(df.monadic.new, "./rds_sets/df.monadic.new.rds")
df.monadic.new <- readRDS("./rds_sets/df.monadic.new.rds")

# COW: COW Country Codes
data.COW <- read.csv("data_sets/COW_country_codes.csv")
data.COW <- unique(data.COW)

# TMK: Targeted Mass Killings
data.TMK <- read.csv("data_sets/tmk_annual_release.csv")
df.monadic <- data.TMK[c('ccode', 'year', 'tmk.onset', 'tmk.ongoing')]
df.monadic$tmk.ongoing <- ifelse(df.monadic$tmk.ongoing >= 1, 1, 0)
df.monadic$tmk.future <- 0

keys.monadic <- expand.grid(unique(data.COW$CCode), (min(df.monadic$year):max(df.monadic$year)))
df.monadic <- merge(x=df.monadic,
                    y=keys.monadic,
                    by.x=c('ccode', 'year'),
                    by.y=c('Var1', 'Var2'),
                    all.y=TRUE)

df.monadic <- df.monadic %>%
  mutate(tmk.future = replace_na(tmk.future, 0)) %>%
  mutate(tmk.onset = replace_na(tmk.onset, 0)) %>%
  mutate(tmk.ongoing = replace_na(tmk.ongoing, 0))

for(curr_year in (min(df.monadic$year)+5):(max(df.monadic$year))) {
  for(ccode in data.COW$CCode) {
    tmk.future <- df.monadic[df.monadic$year == curr_year & df.monadic$ccode == ccode, ]$tmk.onset
    
    if (tmk.future == 1) {
      df.monadic[(curr_year-df.monadic$year) <= 5 & (curr_year-df.monadic$year) > 0 & df.monadic$ccode == ccode, ]$tmk.future <- tmk.future
    }
  }
}

# NMC: National Material Capabilities
data.NMC <- read.csv("data_sets/monadic/NMC_5_0.csv")
df.monadic <- merge(x=df.monadic,
                    y=data.NMC[c('ccode', 'year', 'tpop', 'milper', 'cinc')],
                    by=c('ccode', 'year'),
                    all=TRUE)

df.monadic <- df.monadic %>% mutate(across(c(tpop, milper), na_if, -9))

# ARD: Authoritarian Regime Dataset
data.ARD <- read_xls("data_sets/monadic/ARD_V6.xls")
df.monadic <- merge(x=df.monadic,
                    y=data.ARD[c('cowcode', 'year', 'ifhpol')],
                    by.x=c('ccode', 'year'),
                    by.y=c('cowcode', 'year'),
                    all=TRUE)

# CWD: COW War Data
data.CWD <- read.csv("data_sets/monadic/INTRA-STATE_State_participants_v5.1.csv")
data.CWD <- data.CWD %>%
  mutate(EndYr1 = if_else(EndYr1<0, 2015L, EndYr1)) %>%
  filter(Intnl == 0)

data.CWD.trans <- data.frame(ccode= numeric(0), year= numeric(0), civil_war=numeric(0))
for (i in 1:nrow(data.CWD)) {
  row <- data.CWD[i,]
  
  for (year in row$"StartYr1":row$"EndYr1") {
    data.CWD.trans[nrow(data.CWD.trans)+1,] <- c(row$"CcodeA", year, 1)
    data.CWD.trans[nrow(data.CWD.trans)+1,] <- c(row$"CcodeB", year, 1)
  }
}

data.CWD.trans <- data.CWD.trans[!(data.CWD.trans$ccode<0),] %>% unique()

df.monadic <- merge(x=df.monadic,
                    y=data.CWD.trans,
                    by=c("ccode","year"),
                    all=TRUE)

# Infant Mortality Rate

data.IMR <- read.dta("data_sets/monadic/IMRJPR2707.dta")
df.monadic <- merge(x=df.monadic,
                    y=data.IMR[c('ccode', 'year', 'IMR')],
                    by=c('ccode', 'year'),
                    all=TRUE)


# EFD: Ethnic Fractionalization Dataset
data.EFD <- read.csv("data_sets/monadic/Historical_Index_of_Ethnic_Fractionalisation_Dataset.csv")
data.EFD <- data.EFD %>% rename("StateNme" = "Country")
data.EFD <- data.EFD %>% distinct(StateNme, Year, .keep_all=TRUE)  

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Bosnia-Herzegovina", "Bosnia and Herzegovina", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Cote d'Ivoire", "Ivory Coast", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Democratic People's Republic of Korea", "North Korea", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Democratic Republic of Congo", "Democratic Republic of the Congo", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Democratic Republic of Vietnam", "Vietnam", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Kyrgyz Republic", "Kyrgyzstan", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Republic of Korea", "South Korea", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Serbia", "Yugoslavia", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="USSR", "Russia", StateNme)
)

data.EFD <- data.EFD %>% mutate(
  StateNme = if_else(StateNme=="Yemen PDR", "Yemen People's Republic", StateNme)
)

data.EFD <- merge(x=data.EFD,
             y=data.COW,
             by=c("StateNme"),
             all.x=TRUE)

# Resolve case where USSR and Russia existed simultaneously in 1991
data.EFD[6532,]$EFindex = (data.EFD[6532,]$EFindex + data.EFD[6543,]$EFindex)/2
data.EFD <- data.EFD[-c(6543),]

df.monadic <- merge(x=df.monadic,
                    y=data.EFD[c("EFindex", "CCode","Year")],
                    by.x=c("ccode","year"),
                    by.y=c("CCode","Year"),
                    all=TRUE)

# Monadic Cleanup
df.monadic <- df.monadic %>% filter(year > 1945 & year < 2015)
keys.monadic <- expand.grid(unique(df.monadic$ccode), (1946:2014))
df.monadic <- merge(x=df.monadic,
                    y=keys.monadic,
                    by.x=c('ccode', 'year'),
                    by.y=c('Var1', 'Var2'),
                    all.y=TRUE)

df.monadic <- df.monadic %>%
  mutate(civil_war = replace_na(civil_war, 0)) %>%
  mutate(tmk.onset = replace_na(tmk.onset, 0)) %>%
  mutate(tmk.ongoing = replace_na(tmk.ongoing, 0))

df.monadic.new <- df.monadic[NULL,]

imputedCountry <- function(i) {
  curr.ccode <- country.keys[i]
  
  df.monadic.curr <- df.monadic[df.monadic$ccode == curr.ccode,]
  
  tryCatch(
    expr = {
      df.monadic.curr.MICE <- suppressWarnings(mice(df.monadic.curr,
                                   remove.collinear = F,
                                   remove.constant = F,
                                   print=F))
      
      df.monadic.curr.MICE.complete <- complete(df.monadic.curr.MICE)
      
      return (df.monadic.curr.MICE.complete)
    },
    error = function(e){
      return(df.monadic.curr)
    }
  )
}

country.keys <- unique(df.monadic$ccode)
for (i in 1:length(country.keys)) {
  df.monadic.new <- rbind(df.monadic.new, imputedCountry(i))
}

df.monadic.missingness <- df.monadic %>%
  summarise_each(list(~ 100*mean(is.na(.))))

df.monadic.new.missingness <- df.monadic.new %>%
  summarise_each(list(~ 100*mean(is.na(.))))
