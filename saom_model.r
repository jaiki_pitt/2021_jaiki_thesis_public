setwd("~/src/thesis_data")
currLib <- "~/src/Rlib"
.libPaths(c(.libPaths(), currLib))
#install.packages(c("reshape2",
#	   "readxl",
#	   "dplyr",
#	   "foreign",
#	   "tidyr",
#	   "devtools",
#	   "mltools",
#	   "data.table",
#	   "igraph",
#	   "sna",
#	   "RSiena",
#	   "PRROC"
#	   ), lib=currLib)
library(reshape2, lib.loc=currLib)
library(readxl, lib.loc=currLib)
library(dplyr, lib.loc=currLib)
library(foreign, lib.loc=currLib)
library(tidyr, lib.loc=currLib)
library(mltools, lib.loc=currLib)
library(data.table, lib.loc=currLib)
library(igraph, lib.loc=currLib)
library(sna, lib.loc=currLib)
library(RSiena, lib.loc=currLib)
library(tibble)
library(parallel)
library(PRROC, lib.loc=currLib)
library(stringr)
library(DCG)

df.monadic <- readRDS("./rds_sets/df.monadic.new.rds")
df.dyadic <- readRDS("./rds_sets/df.dyadic.rds")
data.region <- read.csv("data_sets/country_by_regional.csv")
data.COW <- unique(read.csv("data_sets/COW_country_codes.csv"))

# Combining multiple sanctions into broader categories to obtain denser sanction networks
df.dyadic <- df.dyadic %>% rowwise() %>% mutate(
  sanc.economic = max(sanc.trade, sanc.financial, sanc.travel) %>% as.vector,
  sanc.military = max(sanc.military, sanc.arms) %>% as.vector
) %>% select(-c(sanc.trade, sanc.financial, sanc.travel, sanc.arms))

ccodes.dyad <- unique(append(df.dyadic$state1no, df.dyadic$state2no))
ccodes.mono <- unique(df.monadic$ccode)
ccodes <- intersect(ccodes.dyad, ccodes.mono)

df.monadic <- df.monadic[df.monadic$ccode %in% ccodes, ]

data.COW.merge <- merge(x=data.COW,
                    y=data.region,
                    by.x=c('StateNme'),
                    by.y=c('Country'),
                    all.x=TRUE)

df.monadic <- merge(x=df.monadic,
                    y=data.COW.merge[, (3:4)],
                    by.x=c('ccode'),
                    by.y=c('CCode'),
                    all.x=TRUE)

df.monadic <- df.monadic %>% mutate(
  tpop = (scale(df.monadic$tpop) %>% as.vector),
  milper = (scale(df.monadic$milper) %>% as.vector),
  cinc = (scale(df.monadic$cinc) %>% as.vector),
  ifhpol = -(scale(df.monadic$ifhpol) %>% as.vector),
  IMR = (scale(df.monadic$IMR) %>% as.vector),
  EFindex = (scale(df.monadic$EFindex) %>% as.vector)
)

getAllCountries <- function() {
  countries <- tibble(ccodes)
  names(countries) <- c("ccode")
  
  countries
}

generateRelationshipMatrix <- function(df.dyadic, relationship, start_year, end_year, symmetric=FALSE) {
  ccodes <- unique(append(df.dyadic$state1no, df.dyadic$state2no))

  mat.list = list()
  end_year <- end_year+1
  
  for (curr_year in start_year:end_year) {
    mat <- matrix(0L, nrow=length(ccodes), ncol=length(ccodes))
    dimnames(mat) = list(ccodes, ccodes)
    
    df.dyadic.edgelist <- df.dyadic[df.dyadic$year==curr_year & df.dyadic[relationship] == 1, 1:2] %>% mutate(
      state1no = as.character(state1no),
      state2no = as.character(state2no),
    )
    
    df.dyadic.edgelist.matrix <- as.matrix(df.dyadic.edgelist)
    
    mat[df.dyadic.edgelist.matrix] <- 1
    
    mat.list <- append(mat.list, list(mat))
  }
  
  return(array(unlist(mat.list), dim=c(length(ccodes), length(ccodes), (end_year-start_year))))
}

generateMonadicMatrix <- function(df.monadic, variable, start_year, end_year) {
  ccodes <- unique(df.monadic$ccode)
  
  df <- df.monadic[df.monadic$year >= start_year & df.monadic$year <= end_year, ]
  variable.df <- df[c('ccode', 'year', variable)] %>% pivot_wider(names_from='year', values_from=variable)
  variable.df <- variable.df[,-1]
  variable.mat <- as.matrix(variable.df)
  years <- unique(df$year)
  dimnames(variable.mat) <- list(ccodes, years)
  
  return(variable.mat)
}

generateSienaData <- function(df.monadic, df.dyadic, relevant_countries, start_year, end_year) {
  df.dyadic.restricted <- df.dyadic[df.dyadic$state1no %in% relevant_countries$ccode & df.dyadic$state2no %in% relevant_countries$ccode, ]
  df.monadic.restricted  <- df.monadic[df.monadic$ccode %in% relevant_countries$ccode, ]
  
  # Network Variables
  alliance.matrix <- generateRelationshipMatrix(df.dyadic.restricted, "alliance", start_year, end_year+1)
  alliances <- sienaDependent(alliance.matrix, allowOnly=FALSE)
  
  contiguity.matrix <- generateRelationshipMatrix(df.dyadic.restricted, "contiguity", start_year, end_year+1)
  for (i in 1:length(contiguity.matrix[1,1,])) {
    if (contiguity.matrix[1,1,i] == 0) {
      contiguity.matrix[1,1,i] = NA
      break
    }
  }
  contiguities <- sienaDependent(contiguity.matrix, allowOnly=FALSE)

  high_volume_trade.matrix <- generateRelationshipMatrix(df.dyadic.restricted, "high_volume_trade", start_year, end_year+1, symmetric=TRUE)
  high_volume_trade <- sienaDependent(high_volume_trade.matrix, allowOnly=FALSE)
  
  high_dependence_trade.matrix <- generateRelationshipMatrix(df.dyadic.restricted, "high_dependence_trade", start_year, end_year+1)
  high_dependence_trade <- sienaDependent(high_dependence_trade.matrix, allowOnly=FALSE)
  
  rebel_support.matrix <- generateRelationshipMatrix(df.dyadic.restricted, "rebel_support", start_year, end_year+1)
  rebel_support <- sienaDependent(rebel_support.matrix, allowOnly=FALSE)
  
  sanc_economic.matrix <- generateRelationshipMatrix(df.dyadic.restricted, "sanc.economic", start_year, end_year+1)
  sanc_economic <- sienaDependent(sanc_economic.matrix, allowOnly=FALSE)

  sanc_military.matrix <- generateRelationshipMatrix(df.dyadic.restricted, "sanc.military", start_year, end_year+1)
  sanc_military <- sienaDependent(sanc_military.matrix, allowOnly=FALSE)
  
  # Monadic Variables
  tmk_future.matrix <- generateMonadicMatrix(df.monadic.restricted, "tmk.future", start_year, end_year+1)
  tmk_future <- sienaDependent(tmk_future.matrix, type = 'behavior', allowOnly=FALSE)
  
  
  tmk_ongoing.matrix <- generateMonadicMatrix(df.monadic.restricted, "tmk.ongoing", start_year, end_year)
  tmk_ongoing <- varCovar(tmk_ongoing.matrix)
  
  nmc.matrix <- generateMonadicMatrix(df.monadic.restricted, "cinc", start_year, end_year)
  nmc <- varCovar(nmc.matrix)
  
  tpop.matrix <- generateMonadicMatrix(df.monadic.restricted, "tpop", start_year, end_year)
  tpop <- varCovar(tpop.matrix)

  milper.matrix <- generateMonadicMatrix(df.monadic.restricted, "milper", start_year, end_year)
  milper <- varCovar(tpop.matrix)
    
  ifhpol.matrix <- generateMonadicMatrix(df.monadic.restricted, "ifhpol", start_year, end_year)
  ifhpol <- varCovar(ifhpol.matrix)
  
  civil_war.matrix <- generateMonadicMatrix(df.monadic.restricted, "civil_war", start_year, end_year)
  civil_war <- varCovar(civil_war.matrix)
  
  IMR.matrix <- generateMonadicMatrix(df.monadic.restricted, "IMR", start_year, end_year)
  IMR <- varCovar(IMR.matrix)
  
  EFindex.matrix <- generateMonadicMatrix(df.monadic.restricted, "EFindex", start_year, end_year)
  EFindex <- varCovar(EFindex.matrix)
  
  data <- sienaDataCreate(
    alliances,
    contiguities,
    high_volume_trade,
    high_dependence_trade,
    rebel_support,
    sanc_economic,
    sanc_military,
    tmk_future,
    tmk_ongoing,
    nmc,
    tpop,
    ifhpol,
    civil_war,
    IMR,
    EFindex,
    milper
  )
}

generateSienaEffects <- function(data) {
  effects <- getEffects(data)
  
  # Network Dynamics
  effects <- includeEffects(effects, name="alliances", density)
  effects <- includeEffects(effects, name="alliances", inPop)
#  effects <- includeEffects(effects, name="alliances", outAct)
  effects <- includeEffects(effects, name="alliances", gwesp)

  effects <- includeEffects(effects, name="high_volume_trade", density)
  effects <- includeEffects(effects, name="high_volume_trade", inPop)
#  effects <- includeEffects(effects, name="high_volume_trade", outAct)
  effects <- includeEffects(effects, name="high_volume_trade", gwesp)

  effects <- includeEffects(effects, name="high_dependence_trade", density)
  effects <- includeEffects(effects, name="high_dependence_trade", recip)
  effects <- includeEffects(effects, name="high_dependence_trade", inPop)
  effects <- includeEffects(effects, name="high_dependence_trade", outPop)

  effects <- includeEffects(effects, name="high_dependence_trade", outAct)
  effects <- includeEffects(effects, name="rebel_support", density)
  effects <- includeEffects(effects, name="rebel_support", recip)
  effects <- includeEffects(effects, name="rebel_support", inPop)
  effects <- includeEffects(effects, name="rebel_support", outPop)
  effects <- includeEffects(effects, name="rebel_support", outAct)

  effects <- includeEffects(effects, name="sanc_economic", density)
  effects <- includeEffects(effects, name="sanc_economic", recip)
  effects <- includeEffects(effects, name="sanc_economic", inPop)
  effects <- includeEffects(effects, name="sanc_economic", outPop)
  effects <- includeEffects(effects, name="sanc_economic", outAct)

  effects <- includeEffects(effects, name="sanc_military", density)
#  effects <- includeEffects(effects, name="sanc_military", recip)
  effects <- includeEffects(effects, name="sanc_military", inPop)
  effects <- includeEffects(effects, name="sanc_military", outPop)
  effects <- includeEffects(effects, name="sanc_military", outAct)
    
  # Behaviour Shape Effects
  effects <- includeEffects(effects, name="tmk_future", linear)

  # Network Effects: Trade
  #HYP1: Interdependent trade relationships increase the chance of TMK onset.
  effects <- includeEffects(effects, name="tmk_future", recipDeg, interaction1="high_dependence_trade")

  #HYP2: High volume trade relationships decrease the chance of TMK onset.
  effects <- includeEffects(effects, name="tmk_future", outdeg, interaction1="high_volume_trade")
  
  #HYP3: Trade relationships with nations with strong political rights and civil liberties decrease the chance of TMK onset.
  effects <- includeEffects(effects, name="tmk_future", avXAlt, interaction1="ifhpol", interaction2="high_dependence_trade")
  effects <- includeEffects(effects, name="tmk_future", avXAlt, interaction1="ifhpol", interaction2="high_volume_trade")
  
  # Network Effects: Alliance
  #HYP4: Military alliances with nations that have strong political rights and civil liberties decrease the chance of TMK onset.
  effects <- includeEffects(effects, name="tmk_future", avXAlt, interaction1="ifhpol", interaction2="alliances")
  
  #HYP5: Military alliances in general decrease the chance of TMK onset.
  effects <- includeEffects(effects, name="tmk_future", outdeg, interaction1="alliances")
  
  # Network Effects: Contiguity
  #HYP6: Geographical direct contiguity with nations that have strong human rights inhibit TMK onset.
  effects <- includeEffects(effects, name="tmk_future", avXAlt, interaction1="ifhpol", interaction2="contiguities")
  
  #HYP7: Geographical direct contiguity with nations experiencing an internal conflict (Civil War or TMK) increase chances of TMK onset.
  effects <- includeEffects(effects, name="tmk_future", totXAlt, interaction1="civil_war", interaction2="contiguities")
  effects <- includeEffects(effects, name="tmk_future", totXAlt, interaction1="tmk_ongoing", interaction2="contiguities")
  
  # Rebel support
  #HYP8: Nations with indegrees of rebel support from strong foreign states increases the chances of TMK onset.
  effects <- includeEffects(effects, name="tmk_future", indeg, interaction1="rebel_support")
  effects <- includeEffects(effects, name="tmk_future", totXInAlt, interaction1="nmc", interaction2="rebel_support")
  
  # Economic Sanctions
  effects <- includeEffects(effects, name="tmk_future", indeg, interaction1="sanc_economic")
  effects <- includeEffects(effects, name="tmk_future", totXInAlt, interaction1="nmc", interaction2="sanc_economic")
  
  # Military Sanctions
  effects <- includeEffects(effects, name="tmk_future", indeg, interaction1="sanc_military")
  effects <- includeEffects(effects, name="tmk_future", totXInAlt, interaction1="nmc", interaction2="sanc_military")
  
  # Local Influences on behavior
  effects <- includeEffects(effects, name='tmk_future', effFrom, interaction1 = "nmc")
  effects <- includeEffects(effects, name='tmk_future', effFrom, interaction1 = "milper")
  # effects <- includeEffects(effects, name='tmk_future', effFrom, interaction1 = "tpop")
  effects <- includeEffects(effects, name='tmk_future', effFrom, interaction1 = "ifhpol")
  # effects <- includeEffects(effects, name='tmk_future', effFrom, interaction1 = "civil_war")
  effects <- includeEffects(effects, name='tmk_future', effFrom, interaction1 = "IMR")
  effects <- includeEffects(effects, name='tmk_future', effFrom, interaction1 = "EFindex")
  
  for (curr_period in 1:(data$observations-1)) {
    effects <- setEffect(effects, Rate, name="contiguities", type="rate", fix=T, test=F, period=curr_period, initialValue=0.0001)
  }
  
  return (effects)
}

generateClusters <- function() {
  n.clus <- detectCores()-2
  cl = makeCluster(n.clus)
  clusterEvalQ(cl, .libPaths(c(.libPaths(), "~/src/Rlib")))
  clusterEvalQ(cl, library(RSiena, lib.loc="~/src/Rlib"))
  
  return (cl)
}

siena07ToConvergence <- function(algo, data, effects, prev) {
  numr <- 0
  
  cl <- generateClusters()
  ans <- siena07(algo, data=data, effects=effects, batch=TRUE, cl=cl, prevAns=prev) # the first run
  return(ans)
  # bestAns <- ans
  # repeat {
  #   numr <- numr+1 # count number of repeated runs
  #   tm <- ans$tconv.max # convergence indicator
  #   cat(numr, tm,"\n") # report how far we are
  #   
  #   if (!is.na(tm)) {
  #     if (tm < 0.25) {break} # success
  #     if (tm > 10) {break} # divergence without much hope
  #     
  #     if (is.null(bestAns) || tm < bestAns$tconv.max) {
  #       bestAns <- ans
  #     }
  #   }
  #   
  #   if (numr > 0) {break} # now it has lasted too long
  #   
  #   cl <- generateClusters()
  #   ans <- siena07(algo, data=data, effects=effects, batch=TRUE, cl=cl, prevAns=ans)
  # }
  # 
  # return(bestAns)
}

# Function for creating PR curve data out of behaviour extraction results
getbehPR <- function(results) {
  pred <- results[["Joint"]][["Simulations"]]
  # count frequency of predictions (to convert to probability)
  freq <- t(apply(pred, 2, function(x) table(factor(x, levels=0:3))))
  pred.neg <- freq[,1]/1000
  pred.pos <- rowSums(freq[,2:4])/1000
  obsv <- t(results[["Joint"]][["Observations"]])
  # compare probability with true results
  # store curve
  pred.tp <- pred.pos[obsv[,1]>0]
  pred.fp <- pred.pos[obsv[,1]==0]
  pr <- pr.curve(pred.tp, pred.fp, curve=TRUE)
  roc <- roc.curve(pred.tp, pred.fp, curve=TRUE)
  
  return(list(pr, roc))
}

start_year <- as.numeric(commandArgs(trailingOnly=TRUE)[1])

prev <- NULL

possible.file_name <- paste("./model_results/sim_train(",start_year,").rds",sep="")
if (file.exists(possible.file_name)) {
	print("Year already covered")
	quit()
}

tryCatch(
  expr={
    model_results <- list.files("./model_results")
    model_results <- model_results[grepl("sim_train(.+).rds", model_results)]
	most_recent_result <- model_results[model_results < paste("sim_train(",start_year,").rds",sep="")][-1]
	
    prev.file_name <- paste("./model_results/",most_recent_result,sep="")
	print("prev.file_name")
	print(prev.file_name)
    prev <- readRDS(prev.file_name)
    prev
  },
  error = function(e) {
    print(e)
    prev <- NULL
  }
)

training_length <- 20
test_length <- 5
end_year <- start_year+training_length+test_length

relevant_countries <- getAllCountries()

data.train <- generateSienaData(df.monadic, df.dyadic, relevant_countries, start_year, start_year+training_length)
effects.train <- generateSienaEffects(data.train)
algo.train <- sienaAlgorithmCreate(projname = "algorithm_train", n3=5000)

sim.train <- siena07ToConvergence(algo.train, data.train, effects.train, prev)
prev <- sim.train
sim.train
sim.train.name <- paste("./model_results/sim_train(",start_year,").rds", sep="")
saveRDS(sim.train, sim.train.name)

data.test <- generateSienaData(df.monadic, df.dyadic, relevant_countries, start_year+training_length+1, start_year+training_length+test_length)
effects.test <- generateSienaEffects(data.test)
algo.test <- sienaAlgorithmCreate(projname = "algorithm_test", cond=FALSE, useStdInits=FALSE, nsub=0, simOnly=TRUE)

cl <- generateClusters()
sim.test <- siena07(algo.test, data=data.test, effects=effects.test, batch=TRUE, cl=cl, returnDeps=TRUE, prevAns=sim.train)
sim.test

behaviour <- sienaGOF(sim.test, behaviorExtraction, varName="tmk_future")
results <- getbehPR(behaviour)
results



