setwd("~/src/thesis_data")

library(dplyr)

df.monadic <- readRDS("./rds_sets/df.monadic.new.rds")
df.dyadic <- readRDS("./rds_sets/df.dyadic.rds")
data.region <- read.csv("data_sets/country_by_regional.csv")
data.COW <- unique(read.csv("data_sets/COW_country_codes.csv"))

ccodes <- unique(append(df.dyadic$state1no, df.dyadic$state2no))
df.monadic <- df.monadic[df.monadic$ccode %in% ccodes, ]

# Combining multiple sanctions into broader categories to obtain denser sanction networks
df.dyadic <- df.dyadic %>% rowwise() %>% mutate(
  sanc.economic = max(sanc.trade, sanc.financial, sanc.travel) %>% as.vector,
  sanc.military = max(sanc.military, sanc.arms) %>% as.vector
) %>% select(-c(sanc.trade, sanc.financial, sanc.travel, sanc.arms)) %>% as.data.frame

df.monadic <- df.monadic %>% mutate(
  tpop = (scale(df.monadic$tpop) %>% as.vector),
  milper = (scale(df.monadic$milper) %>% as.vector),
  cinc = (scale(df.monadic$cinc) %>% as.vector),
  ifhpol = -(scale(df.monadic$ifhpol) %>% as.vector),
  IMR = (scale(df.monadic$IMR) %>% as.vector),
  EFindex = (scale(df.monadic$EFindex) %>% as.vector)
)

effectIndegrees <- function(df.dyadic, ccode, year, network) {
  indegrees <- sum(df.dyadic[df.dyadic$state2no == ccode &
                               df.dyadic$year == year, network])
  
  indegrees
}

effectOutdegrees <- function(df.dyadic, ccode, year, network) {
  outdegrees <- sum(df.dyadic[df.dyadic$state1no == ccode &
                                df.dyadic$year == year, network])
  
  outdegrees
}

effectRecipDegrees <- function(df.dyadic, ccode, year, network) {
  df.relevant <- df.dyadic[
    (df.dyadic$state1no == ccode | 
       df.dyadic$state2no == ccode) &
      df.dyadic$year == year,]
  
  df.relevant.A <- df.relevant[df.relevant$state1no != 2, -c(2)]
  df.relevant.B <- df.relevant[df.relevant$state2no != 2, -c(1)]
  
  df.relevant.merge <- merge(
    x=df.relevant.A,
    y=df.relevant.B,
    by.x=c("state1no"),
    by.y=c("state2no")
  )
  
  networkX <- paste(network, ".x", sep="")
  networkY <- paste(network, ".y", sep="")
  
  df.recip <- df.relevant.merge[df.relevant.merge[,networkX] == 1 &
                                  df.relevant.merge[,networkY] == 1,]
  
  recipDegrees <- nrow(df.recip)
  
  recipDegrees
}

effectTotXAlt <- function(df.dyadic, df.monadic, ccode, year, network, variable) {
  df.dyadic <- merge(x=df.dyadic,
                     y=df.monadic,
                     by.x=c('state2no', 'year'),
                     by.y=c('ccode', 'year'))
  
  totXAlt <- sum(df.dyadic[df.dyadic$state1no == ccode &
                             df.dyadic$year == year &
                             df.dyadic[,network] == 1, variable], na.rm=TRUE)
  
  totXAlt
}

effectTotXInAlt <- function(df.dyadic, df.monadic, ccode, year, network, variable) {
  df.dyadic <- merge(x=df.dyadic,
                     y=df.monadic,
                     by.x=c('state2no', 'year'),
                     by.y=c('ccode', 'year'))
  
  totXAlt <- sum(df.dyadic[df.dyadic$state2no == ccode &
                             df.dyadic$year == year &
                             df.dyadic[,network] == 1, variable], na.rm=TRUE)
  
  totXAlt
}

effectAvXAlt <- function(df.dyadic, df.monadic, ccode, year, network, variable) {
  totXAlt <- effectTotXAlt(df.dyadic, df.monadic, ccode, year, network, variable)
  outdegree <- effectOutdegrees(df.dyadic, ccode, year, network)
  
  totXAlt/outdegree
}

effect <- as.numeric(commandArgs(trailingOnly=TRUE)[1])


if (effect == 1) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    indeg_alliance = effectIndegrees(df.dyadic, ccode, year, "alliance"),
  )
} else if (effect == 2) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    recip_high_dependence_trade = effectRecipDegrees(df.dyadic, ccode, year, "high_dependence_trade"),
  )
} else if (effect == 3) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    outdeg_high_volume_trade = effectOutdegrees(df.dyadic, ccode, year, "high_volume_trade"),
  )
} else if (effect == 4) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    avAlt_ifhpol_high_dependence_trade = effectAvXAlt(df.dyadic, df.monadic, ccode, year, "high_dependence_trade", "ifhpol"),
  )
} else if (effect == 5) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    avAlt_ifhpol_high_volume_trade = effectAvXAlt(df.dyadic, df.monadic, ccode, year, "high_volume_trade", "ifhpol"),
  )
} else if (effect == 6) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    avAlt_ifhpol_alliance = effectAvXAlt(df.dyadic, df.monadic, ccode, year, "alliance", "ifhpol"),
  )
} else if (effect == 7) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    outdeg_alliance = effectOutdegrees(df.dyadic, ccode, year, "alliance"),
  )
} else if (effect == 8) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    avAlt_ifhpol_contiguity = effectAvXAlt(df.dyadic, df.monadic, ccode, year, "contiguity", "ifhpol"),
  )
} else if (effect == 9) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    totAlt_civil_war_contiguity = effectTotXAlt(df.dyadic, df.monadic, ccode, year, "contiguity", "civil_war"),
  )
} else if (effect == 10) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    indeg_rebel_support = effectIndegrees(df.dyadic, ccode, year, "rebel_support"),
  )
} else if (effect == 11) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    totInAlt_nmc_rebel_support = effectTotXInAlt(df.dyadic, df.monadic, ccode, year, "rebel_support", "cinc"),
  )
} else if (effect == 12) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    indeg_sanc.military = effectIndegrees(df.dyadic, ccode, year, "sanc.military"),
  )
} else if (effect == 13) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    totInAlt_nmc_sanc.military = effectTotXInAlt(df.dyadic, df.monadic, ccode, year, "sanc.military", "cinc"),
  )
} else if (effect == 14) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    indeg_sanc.economic = effectIndegrees(df.dyadic, ccode, year, "sanc.economic"),
  )
} else if (effect == 15) {
  df.monadic.with_dyad <- df.monadic %>% rowwise() %>% mutate(
    totInAlt_nmc_sanc.economic = effectTotXInAlt(df.dyadic, df.monadic, ccode, year, "sanc.economic", "cinc")
  )
}

saveRDS(df.monadic.with_dyad, paste("./rds_sets/monadic_plus_dyadic/df.monadic.with_dyad_", effect, ".rds", sep=""))


